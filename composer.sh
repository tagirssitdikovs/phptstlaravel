#!/usr/bin/env bash

set -e

echo Starting services

#rm -f bootstrap/cache/*.php
#php artisan cache:clear && echo Artisan cache clear
#php artisan view:clear && echo view:clear

#docker stop $(docker ps -a -q)
#docker rm $(docker ps -a -q)
#docker rmi -f $(docker images -q)

#sudo docker rmi $(sudo docker images --filter=reference='phptest_app' --format "{{.ID}}")

#docker-compose down -v
docker-compose build
docker-compose up -d
docker-compose exec app chmod -Rf 777 /var/www/storage && echo Set permissions for /var/www/storage

sleep 5

echo Seeding database

read -p "Press enter to continue"

echo Database migrating ...
#docker-compose exec app php artisan migrate && echo Database migrated

sleep 5
echo Database seeding ...
#docker-compose exec app php artisan db:seed && echo Database seeded

echo Finish
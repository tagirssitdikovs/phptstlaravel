<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/tasks', 'TaskController@store')->name('store');

Route::get('/tasks', 'TaskController@index')->name('index');

Route::get('/tasks/{task_id}/comments', 'TaskController@showcomments')->name('showcomments');

//Route::post('/comments', 'TaskController@storecomment')->name('storecomment');
Route::post('/tasks/{task_id}/comments', 'TaskController@storecomment')->name('storecomment');
//
//Route::post('/tasks/1/comments', ['as' => 'storecomment', 'uses' => 'TaskController@storecomment']);

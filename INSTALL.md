https://github.com/assurrussa/grid-view-table-app

https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
https://docs.docker.com/compose/install/

composer global require laravel/installer

sudo xed /etc/environment
PATH="/home/tagir/.config/composer/vendor/bin:............

cd ~/IdeaProjects
composer create-project --prefer-dist laravel/laravel phptst

Intellij Idea
=============

composer require assurrussa/grid-view-table

apt-get install php7.2-mbstring
apt-get install php7.2-xml
apt-get install php7.2-zip

composer install

php artisan make:model Task -c -r

Update:
/app/Task.php
/app/Http/Controllers/TaskController.php

Add to /resources/views/layouts/app.blade.php:
    <link href="{{ asset('vendor/grid-view/css/amigrid.css') }}" rel="stylesheet">
</head>
. . .
    {{--<script src="{{ asset('vendor/grid-view/js/amigrid.js') }}"></script>--}}
    <script src="{{ asset('vendor/grid-view/js/amigrid-full.js') }}"></script>
    @stack('scripts')
</body>





Development:
============

php artisan make:migration create_task_table --create=task

Update /database/migrations/2018_11_20_164751_create_task_table.php:
            $table->string('user');
            $table->string('assignee');
            $table->string('comment');


php artisan make:auth














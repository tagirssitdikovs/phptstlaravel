<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{
    private $task;

    /**
     * TaskController constructor.
     *
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
        $this->middleware('auth', ['except' => ['store','storecomment']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->getGrid()->getSimple();
        if (request()->ajax() || request()->wantsJson()) {
            return $data->toHtml();
        }

        return view('tasks.index', compact('title', 'data'));

    }

    /**
     * @return \Assurrussa\GridView\GridView
     */
    public function getGrid()
    {

        /** @var \Assurrussa\GridView\GridView $gridView */
        $query = $this->task->newQuery();
        $gridView = app('amiGrid');
        $gridView->setQuery($query)
            ->setSortName('user')
            ->setSearchInput(true);

        // columns
        $gridView->column('id', '#')->setSort(true)->setFilterString('byId', '', '', 'width:60px');
        $gridView->column('user', 'user')->setFilterString('byUserLike', '', '', 'width:60px')->setSort(true);
        $gridView->column('assignee', 'assignee')->setFilterString('byAssigneeLike', '', '', 'width:160px')->setSort(false);
        $gridView->column('comment', 'comment')->setFilterString('byCommentLike', '', '', 'width:60px')->setSort(false);
        $gridView->column('created_at', 'Created At')->setDateActive(true)
            ->setFilterDate('byCreatedAt', '', true, 'Y-m-d H:i')
            ->setFilterFormat('DD MMM YY');
        $gridView->column('updated_at', 'Updated At')->setDateActive(true)
            ->setFilterDate('byUpdatedAt', '', true, 'Y-m-d H:i')
            ->setFilterFormat('DD MMM YY');

        return $gridView;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $taskresp = Task::create($request->all());
        return response()->json($taskresp->all(), 200);
    }

    public function showcomments($task_id)
    {
        $taskrec = Task::all()->find(['id' => $task_id]);
        return response()->json($taskrec->all(), 200);
    }

    public function storecomment(Request $request)
    {
        $task_id = Input::route()->parameters()['task_id'];
        $taskrec = Task::find($task_id);
//        $taskrec = Task::all()->find(['id' => $task_id]);
        $taskrec->comment = $request->comment;
        $taskresp = $taskrec->save();
//        $taskresp = $taskrec->save($request->all());
        return response()->json($taskresp, 200);
    }

}

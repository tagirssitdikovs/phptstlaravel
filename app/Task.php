<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';
    public $timestamps = false;
    protected $fillable = ['user', 'assignee', 'comment', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected $casts = [
        'id'           => 'integer',
        'user'         => 'string',
        'assignee'     => 'string',
        'created_at'   => 'datetime',
        'updated_at'   => 'datetime',
    ];

    /**
     * @param Task $query
     * @param int  $int
     *
     * @return mixed
     */
    public function scopeById($query, $int)
    {
        return $query->where('id', '=', $int);
    }

    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByUser($query, $string)
    {
        return $query->where('user', '=', $string);
    }

    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByUserLike($query, $string)
    {
        return $query->where('user', 'like', $string . '%');
    }


    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByAssignee($query, $string)
    {
        return $query->where('assignee', '=', $string);
    }

    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByAssigneeLike($query, $string)
    {
        return $query->where('assignee', 'like', $string . '%');
    }

    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByComment($query, $string)
    {
        return $query->where('comment', '=', $string);
    }

    /**
     * @param Task   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByCommentLike($query, $string)
    {
        return $query->where('comment', 'like', $string . '%');
    }

}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'name'     => 'vasja',
            'email'    => 'vasja.pupkin@gmail.com',
            'password' => Hash::make('passw123'),
        ));
    }

}


rm -f bootstrap/cache/*.php
php artisan cache:clear && echo Artisan cache clear
php artisan view:clear && echo view:clear

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi -f $(docker images -q)

docker-compose down -v

docker-compose exec app php /var/www/artisan key:generate

docker-compose exec app php /var/www/artisan optimize

=====================================================================================

docker-compose build

docker-compose up -d

docker-compose exec app chmod -Rf 777 /var/www/storage && echo Set permissions for /var/www/storage

docker-compose exec app php artisan migrate && echo Database migrated

composer dump-autoload

docker-compose exec app php artisan db:seed && echo Database seeded

curl -X POST http://localhost:8080/tasks \
 -H "Accept: application/json" \
 -H "Content-Type: application/json" \
 -d '{"user": "user_04","assignee": "task_04","comment": "comment_04"}'

curl -X POST http://localhost:8080/tasks/2/comments \
 -H "Accept: application/json" \
 -H "Content-Type: application/json" \
 -d '{"comment": "comment_88"}'

http://localhost:8080/tasks/1/comments

docker-compose logs app
docker-compose logs database








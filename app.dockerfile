FROM php:7.2-fpm
WORKDIR /var/www


RUN apt-get update && apt-get install -y libmcrypt-dev \
    mysql-client libmagickwand-dev --no-install-recommends

RUN docker-php-ext-install pdo_mysql
RUN apt-get update -y && apt-get install -y openssl zip unzip

RUN apt-get install git -y
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | /usr/local/bin/php -- --filename=composer --install-dir=/usr/local/bin
ADD . /var/www
